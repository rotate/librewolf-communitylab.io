---
title: Installation
---

## Linux

### Arch

The AUR has [`librewolf`](https://aur.archlinux.org/packages/librewolf) and [`librewolf-bin`](https://aur.archlinux.org/packages/librewolf-bin) packages. The first one compiles Firefox with our patches from source while the latter provides a binary.

With `yay`:

```sh
yay -S librewolf

# alternatively:
yay -S librewolf-bin
```

Without an AUR helper:

```sh
git clone https://aur.archlinux.org/librewolf-bin.git librewolf
cd librewolf
makepkg -si
```

### Debian-based

We also offer AppImages and snaps.

todo: add more details.

### Gentoo

See the [LibreWolf Gentoo repo](https://gitlab.com/librewolf-community/browser/gentoo#readme).

### Compiling from source

todo.

## macOS

The macOS installation process involves running a script that patches a Firefox disk image to create a LibreWolf disk image. The macOS build process doesn't build Firefox from source yet, but we may try to do that in the future.

```sh
git clone https://gitlab.com/librewolf-community/browser/macos.git
cd macos
./package.sh
```

See the [LibreWolf macOS repo](https://gitlab.com/librewolf-community/browser/macos) for more.

## Windows

Unfortuantely, we don't offer Windows builds yet. If you can help with these, please get in touch!
